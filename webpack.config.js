let HtmlWebpackPlugin = require('html-webpack-plugin');
var HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');


const path = require("path")

module.exports = {
    entry:{
        p1:"./src/index.js",
        p2:"./src/index2.js"
    },
    output:{
        path:path.join(__dirname,"dist"),
        filename:"bundle_[name].js"
    },
    mode:"production",
    plugins:[

        new HtmlWebpackPlugin(
            {
                template: 'src/tpl/index.html',   
                filename: 'index.html',
                inlineSource: '.(js|css)'
            }
        ),
        new HtmlWebpackInlineSourcePlugin()
    ]
}